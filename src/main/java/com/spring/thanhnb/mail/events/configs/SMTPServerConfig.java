package com.spring.thanhnb.mail.events.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.subethamail.smtp.auth.EasyAuthenticationHandlerFactory;
import org.subethamail.smtp.auth.UsernamePasswordValidator;
import org.subethamail.smtp.helper.SimpleMessageListener;
import org.subethamail.smtp.server.SMTPServer;

@Component
public class SMTPServerConfig {

    public SMTPServerConfig(SimpleMessageListener marketingMsgListener) {
        System.out.println("---------- START SMTPServerConfig -------------");
        UsernamePasswordValidator authValidator = new SimpleAuthValidatorImpl();
        EasyAuthenticationHandlerFactory easyAuth = new EasyAuthenticationHandlerFactory(authValidator);

        SMTPServer smtpServer = SMTPServer
                .port(25000)
                .hostName("smtp.gmail.com")
                .simpleMessageListener(marketingMsgListener)
                .requireAuth(true)
                .authenticationHandlerFactory(easyAuth)
                .build();

        smtpServer.start();
        System.out.println("---------- END SMTPServerConfig -------------");
    }
}
