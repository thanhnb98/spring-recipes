package com.spring.thanhnb.mail.events.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.thanhnb.mail.events.model.ReceivedMail;
import org.springframework.stereotype.Component;
import org.subethamail.smtp.TooMuchDataException;
import org.subethamail.smtp.helper.SimpleMessageListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
public class MailListener implements SimpleMessageListener {

    private static final List<String> MAIL_DOMAINS = Arrays.asList("@gmail.com", "@nganluong.vn", "@iist.vn");

    @Override
    public boolean accept(String from, String recipient) {
        System.out.println("------ START ACCEPT METHOD --------");
        try {
            return isMatchMailDomain(recipient);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void deliver(String from, String recipient, InputStream inputStream) throws TooMuchDataException, IOException {
        System.out.println("deliver message of MARKETING department");
        System.out.println("From : " + from);
        System.out.println("Recipient : " + recipient);

        System.out.println(new ObjectMapper().writeValueAsString(ReceivedMail.getReceivedMail(inputStream)));
    }

    private boolean isMatchMailDomain(String mail) throws IllegalAccessException {
        if (!isNullOrEmpty(mail)) {
            for (String domain : MAIL_DOMAINS) {
                return mail.endsWith(domain);
            }
        }
        throw new IllegalAccessException("mail required");
    }

    private boolean isNullOrEmpty(String s) {
        return Objects.isNull(s) || "".equals(s);
    }
}
