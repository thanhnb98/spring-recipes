package com.spring.thanhnb.mail.events.configs;

import org.springframework.stereotype.Component;
import org.subethamail.smtp.MessageContext;
import org.subethamail.smtp.auth.LoginFailedException;
import org.subethamail.smtp.auth.UsernamePasswordValidator;

@Component
public class SimpleAuthValidatorImpl implements UsernamePasswordValidator {

    private final String CREDENTIALS_LOGIN = "thanhnb.iist@gmail.com";
    private final String CREDENTIALS_PASSWORD = "thanhdsa1";

    @Override
    public void login(String username, String password, MessageContext messageContext) throws LoginFailedException {
        if (CREDENTIALS_LOGIN.equals(username) && CREDENTIALS_PASSWORD.equals(password)) {
            System.out.println("Authenticated successfully");
        } else {
            System.err.println("Invalid authentication !");
            throw new LoginFailedException();
        }
    }
}
