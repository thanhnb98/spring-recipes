package com.spring.thanhnb.mail.read_inbox.thread_2;
import com.spring.thanhnb.mail.read_inbox.GlobalConfig;
import com.spring.thanhnb.mail.read_inbox.models.MailConfigInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

@Configuration
@EnableScheduling
public class ScheduleMail2 {

    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    @Autowired
    private ApplicationContext applicationContext;

//    @Scheduled(cron = "0 * * * * *")
    public void fetchEmail() {
        String code = "NGL" + System.currentTimeMillis();
        System.out.println("-------------- START CRON JOB: " + code + "--------------------------");
        List<MailConfigInfo> mails = Arrays.asList(
                new MailConfigInfo(GlobalConfig.USER_EMAIL1, GlobalConfig.PASS_EMAIL1),
                new MailConfigInfo(GlobalConfig.USER_EMAIL2, GlobalConfig.PASS_EMAIL2));

        for (MailConfigInfo mail : mails) {
            FetchMailTask2 fetchMailTask2 = new FetchMailTask2(mail);
            applicationContext.getAutowireCapableBeanFactory().autowireBean(fetchMailTask2);
            this.threadPoolExecutor.submit(fetchMailTask2);
        }
        System.out.println("-------------- END CRON JOB: " + code + "--------------------------");
    }
}
