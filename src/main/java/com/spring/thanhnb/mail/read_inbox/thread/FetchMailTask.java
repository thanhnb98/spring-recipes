package com.spring.thanhnb.mail.read_inbox.thread;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.thanhnb.mail.read_inbox.MailConfig;
import com.spring.thanhnb.mail.read_inbox.models.MailConfigInfo;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.*;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.ReceivedDateTerm;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class FetchMailTask implements Runnable {

    private MailConfigInfo mailConfigInfo;

    public FetchMailTask(MailConfigInfo mailConfigInfo) {
        this.mailConfigInfo = mailConfigInfo;
    }

    @Autowired
    private MailConfig mailConfig;

    @Override
    public void run() {
        System.out.println("---------------------- START THREAD: " + Thread.currentThread().getName() + "--------------");
        readInboxMail(this.mailConfigInfo);
        System.out.println("---------------------- END THREAD: " + Thread.currentThread().getName() + "--------------");
    }


    private void readInboxMail(MailConfigInfo mailConfigInfo) {
        System.out.println("----------- START READ INBOX MAIL: " + mailConfigInfo.getEmail() + "-----------");
        try {
            System.out.println(new ObjectMapper().writeValueAsString(mailConfigInfo));
            Store store = getStore(mailConfigInfo);
            Folder inbox = store.getFolder("inbox");
            inbox.open(Folder.READ_ONLY);
            int messageCount = inbox.getMessageCount();
            System.out.println("Total Messages:- " + messageCount);

            Calendar cal = Calendar.getInstance();
            cal.roll(Calendar.MONTH, false);

            Flags seen = new Flags(Flags.Flag.SEEN);
            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);

            Message[] messages = inbox.search(new ReceivedDateTerm(ComparisonTerm.GT, cal.getTime()));

            System.out.println("------------------------------");
            System.out.println("------------------------------" + messages.length + "------------------------");

            for (int i = 0; i < messages.length; i++) {
                System.out.println("Mail Subject:- " + i + ": " + messages[i].getSubject());
//                System.out.println("Mail Flag:- " + i + ": " + (isSeenMail(messages[i].getFlags()) ? "Da xem" : "Chua xem"));
//                System.out.println("Mail ReceivedDate:- " + i + ": " + messages[i].getReceivedDate());
            }

            inbox.close(true);
            store.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("----------- END READ INBOX MAIL: " + mailConfigInfo.getEmail() + "-----------");
    }

    private boolean isSeenMail(Flags flags) {
        return !Objects.isNull(flags) && flags.getSystemFlags().length != 0;
    }

    private Store getStore(MailConfigInfo mailConfigInfo) throws IOException, MessagingException {
        Session session = Session.getDefaultInstance(mailConfig.getMailConfig(), null);
        Store store = session.getStore("imaps");
        store.connect(mailConfigInfo.getHost(), mailConfigInfo.getEmail(), mailConfigInfo.getPassword());
        return store;
    }
}
