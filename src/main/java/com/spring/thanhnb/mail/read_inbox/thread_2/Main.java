package com.spring.thanhnb.mail.read_inbox.thread_2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        List<Future<ThreadState>> futures = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Future<ThreadState> future = executorService.submit(new Task());
            futures.add(future);
        }

        //blocking
        for (Future<ThreadState> future : futures) {
            try {
                ThreadState threadState = future.get(3, TimeUnit.SECONDS);
                System.out.println("RES: " + new ObjectMapper().writeValueAsString(threadState));
            } catch (TimeoutException | JsonProcessingException e) {
                e.printStackTrace();
                System.out.println("TIME OUT");
            }
        }
        executorService.shutdown();
    }
}
