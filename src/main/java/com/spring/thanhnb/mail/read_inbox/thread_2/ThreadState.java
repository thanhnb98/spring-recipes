package com.spring.thanhnb.mail.read_inbox.thread_2;

public class ThreadState {
    private String name;
    private String state;
    private String detail;

    public static ThreadState build(Thread currentThread, String state) {
        ThreadState threadState = new ThreadState();
        threadState.setName(currentThread.getName());
        threadState.setState(state);
        threadState.setDetail(null);
        return threadState;
    }

    public static ThreadState build(Thread currentThread, String state, String detail) {
        ThreadState threadState = new ThreadState();
        threadState.setName(currentThread.getName());
        threadState.setState(state);
        threadState.setDetail(detail);
        return threadState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
