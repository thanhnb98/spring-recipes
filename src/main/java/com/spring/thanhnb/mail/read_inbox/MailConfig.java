package com.spring.thanhnb.mail.read_inbox;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * {@docRoot https://www.technicalkeeda.com/java-tutorials/how-to-access-gmail-inbox-using-java-imap}
 */
@Component
public class MailConfig {

    @Bean
    public Properties getMailConfig() throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(ResourceUtils.getFile("classpath:mail/smtp.properties")));
        return props;
    }
}
