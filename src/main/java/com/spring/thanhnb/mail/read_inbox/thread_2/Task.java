package com.spring.thanhnb.mail.read_inbox.thread_2;

import java.util.concurrent.Callable;

public class Task implements Callable<ThreadState> {
    @Override
    public ThreadState call() throws Exception {
        try {
            System.out.println("---- CALL METHOD: " + Thread.currentThread().getName() + "------");
            Thread.sleep(5000);
            System.out.println("---- CALL METHOD: " + Thread.currentThread().getName() + " OKE ------");
            return build(Thread.currentThread(), "OKE");
        } catch (Exception e) {
            System.out.println("---- CALL METHOD: " + Thread.currentThread().getName() + " ERROR ------");
            return build(Thread.currentThread(), "ERROR", e.getMessage());
        }
    }

    private ThreadState build(Thread currentThread, String state) {
        ThreadState threadState = new ThreadState();
        threadState.setName(currentThread.getName());
        threadState.setState(state);
        threadState.setDetail(null);
        return threadState;
    }

    private ThreadState build(Thread currentThread, String state, String detail) {
        ThreadState threadState = new ThreadState();
        threadState.setName(currentThread.getName());
        threadState.setState(state);
        threadState.setDetail(detail);
        return threadState;
    }
}
