package com.spring.thanhnb.mail.read_inbox.models;

public class MailConfigInfo {

    private static final String DEFAULT_HOST = "smtp.gmail.com";

    private String host;
    private String email;
    private String password;

    public MailConfigInfo(String host, String email, String password) {
        this.host = host;
        this.email = email;
        this.password = password;
    }

    public MailConfigInfo(String email, String password) {
        this.host = DEFAULT_HOST;
        this.email = email;
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
