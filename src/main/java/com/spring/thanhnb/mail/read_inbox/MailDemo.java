package com.spring.thanhnb.mail.read_inbox;

import com.spring.thanhnb.mail.read_inbox.models.MailConfigInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.search.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

@RestController
public class MailDemo {

    @Autowired
    private MailConfig mailConfig;

    @RequestMapping(value = "/demoMail", method = RequestMethod.GET)
    public void demoMail() {
        readInboxMail();
    }


    private void readInboxMail() {
        try {
            Store store = getStore();
            Folder inbox = store.getFolder("inbox");
            inbox.open(Folder.READ_ONLY);
            int messageCount = inbox.getMessageCount();
            System.out.println("Total Messages:- " + messageCount);

            Calendar cal = Calendar.getInstance();
            cal.roll(Calendar.HOUR_OF_DAY, false);

            Flags seen = new Flags(Flags.Flag.SEEN);
            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);

            Message[] messages = inbox.search(new ReceivedDateTerm(ComparisonTerm.GT, cal.getTime()));

            System.out.println("------------------------------");
            System.out.println("------------------------------" + messages.length + "------------------------");

            for (int i = 0; i < messages.length; i++) {
                System.out.println("Mail Subject:- " + i + ": " + messages[i].getSubject());
                System.out.println("Mail Flag:- " + i + ": " + (isSeenMail(messages[i].getFlags()) ? "Da xem" : "Chua xem"));
                System.out.println("Mail ReceivedDate:- " + i + ": " + messages[i].getReceivedDate());
            }

            inbox.close(true);
            store.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isSeenMail(Flags flags) {
        return !Objects.isNull(flags) && flags.getSystemFlags().length != 0;
    }

    private Store getStore() throws IOException, MessagingException {
        Session session = Session.getDefaultInstance(mailConfig.getMailConfig(), null);
        Store store = session.getStore("imaps");
        store.connect("smtp.gmail.com", "thanhnb.iist@gmail.com", "thanhdsa1");
        return store;
    }
}
