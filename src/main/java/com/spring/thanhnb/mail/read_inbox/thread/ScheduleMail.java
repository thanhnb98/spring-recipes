package com.spring.thanhnb.mail.read_inbox.thread;

import com.spring.thanhnb.mail.read_inbox.models.MailConfigInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;


@Configuration
@EnableScheduling
public class ScheduleMail {

    @Qualifier("executorService1")
    @Autowired
    private ExecutorService executorService;

    @Autowired
    private ApplicationContext applicationContext;

//    @Scheduled(cron = "0 * * * * *")
    public void fetchEmail() {
        String code = "NGL" + System.currentTimeMillis();
        System.out.println("-------------- START CRON JOB: " + code + "--------------------------");
        List<MailConfigInfo> mails = Arrays.asList(
                new MailConfigInfo("thanhnb.iist@gmail.com", "abc"),
                new MailConfigInfo("nguyenbathanh1280@gmail.com", "abc"));

        for (MailConfigInfo mail : mails) {
            FetchMailTask fetchMailTask = new FetchMailTask(mail);
            applicationContext.getAutowireCapableBeanFactory().autowireBean(fetchMailTask);
            this.executorService.submit(fetchMailTask);
        }
        System.out.println("-------------- END CRON JOB: " + code + "--------------------------");
    }

}
