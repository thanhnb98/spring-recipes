package com.spring.thanhnb.mail.read_inbox.completable_future;

import java.util.concurrent.CompletableFuture;

public interface IMailService {
    CompletableFuture<String> fetchMail();
}
