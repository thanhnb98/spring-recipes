package com.spring.thanhnb.mail.read_inbox.thread_2;

import com.spring.thanhnb.mail.read_inbox.thread_2.exception_handler.CustomRejectException;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.concurrent.*;

@Component("threadPoolExecutorConfig1")
public class ThreadPoolExecutorConfig {

    private final static int CORE_POOL_SIZE = 10;
    private final static int MAX_POOL_SIZE = 100;
    private final static long KEEP_TIME_ALIVE = 120L;

    @Bean(name = "executorService1")
    public ExecutorService executorService() {
        return new ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE,
                KEEP_TIME_ALIVE, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(300),
                new CustomRejectException());
    }
}
