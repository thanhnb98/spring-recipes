package com.spring.thanhnb.mail.read_inbox.thread;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
public class ThreadPoolExecutorConfig {

    @Bean
    public ThreadPoolExecutor threadPoolExecutor() {
        int corePoolSize = 50;
        int maximumPoolSize = 100;
        int queueCapacity = 100;
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                corePoolSize, // Số corePoolSize
                maximumPoolSize, // số maximumPoolSize
                100, // thời gian một thread được sống nếu không làm gì
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(queueCapacity)); // Blocking queue để cho request đợi
        return executor;
    }
}
