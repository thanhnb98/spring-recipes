package com.spring.thanhnb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThanhnbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThanhnbApplication.class, args);
	}

}
