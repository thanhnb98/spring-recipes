package com.spring.thanhnb.ch02._06;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;


/**
 * Define a @PropertySource annotation with a value of classpath
 * The classpath: prefix tells Spring to loop up for discounts.properties file in the class path
 * <p>
 * - Define the @PropertySource annotation to load the properties file, you also need to define a
 * PropertySourcePlaceholderConfigurer bean with the @Bean annotation, Spring auto map @PropertySource discounts.properties file
 * so it properties can accessible as bean properties
 * <p>
 * - Get value of properties file by syntax is @Value("${key:default_value}").
 */
@Component
@PropertySource("classpath:discounts.properties")
@ComponentScan("com.spring.thanhnb.ch02._06.ShopConfig")
public class ShopConfig {

    @Value("${specialcustomer.discount:0}")
    private double discount;

    @Bean
    public static PropertySourcesPlaceholderConfigurer
    propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    public void getConfigs() {
        System.out.println(this.discount);
    }
}
