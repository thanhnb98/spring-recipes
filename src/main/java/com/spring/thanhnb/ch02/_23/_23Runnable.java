package com.spring.thanhnb.ch02._23;

import java.util.Date;

public class _23Runnable implements Runnable {
    @Override
    public void run() {
        try {
            System.out.println("---- start thread : " + Thread.currentThread().getName() + " -----");
            Thread.sleep(1000);

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("---- thread InterruptedException: " + Thread.currentThread().getName() + " -----");
        }
        System.out.println(Thread.currentThread().getName());
        System.out.printf("Hello at %s \n", new Date());
    }
}
