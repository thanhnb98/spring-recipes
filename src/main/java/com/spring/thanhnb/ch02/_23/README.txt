2-23. Applying Concurrency with Spring and TaskExecutors

#Problem
You want to build a threaded, concurrent program with Spring but don’t know what approach to use since
there’s no standard approach.

#Solution
Use Spring’s TaskExecutor abstraction.

#HOW IT WORK:

- ExecutorService, a subinterface, provides more functionality for managing threads and provides support
to raise events to threads, such as shutdown().
- Many of them are available via static factory methods in the java.util.concurrent
package.
- The ExecutorService class provides a submit() method, which returns a Future<T> object. An instance
of Future<T> can be used to track the progress of a thread that’s usually executing asynchronously. You can call
Future.isDone() or Future.isCancell() to determine whether the job is finished or cancelled. When you use
ExecutorService and submit() inside a Runnable instance whose run method has no return type, calling get() on the
returned Future returns null, or the value specified on submission.

Example:

Runnable task = new Runnable(){
 public void run(){
 try{
    Thread.sleep( 1000 * 60 ) ;
    System.out.println("Done sleeping for a minute, returning! " );
    } catch (Exception ex) { /* ... */ }
 }
};

ExecutorService executorService = Executors.newCachedThreadPool() ;
if(executorService.submit(task, Boolean.TRUE).get().equals( Boolean.TRUE ))
 System.out.println( "Job has finished!");