2-11. Use Spring Environments and Profiles to Load Different Sets of POJOs

#Problem
You want to use the same set of POJO instances or beans but with different instantiation values for different
application scenarios (e.g., production and development and testing).

#Solution


Create a Java Configuration Class with the @Profile Annotation