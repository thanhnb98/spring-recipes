package com.spring.thanhnb.ch02._07;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Objects;

@Component
public class MessageHandler {

    @Autowired
    private MessageSource messageSource;

    public String getMessage(HttpServletRequest request, String keyMessage, String defaultMessage) {
        Locale locale;
        String locateStr = request.getParameter("language");
        if (stringNotNullOrEmpty(locateStr)) {
            locale = new Locale(locateStr, locateStr);
        } else {
            locale = new Locale("vi", "VIE");
        }
        return messageSource.getMessage(keyMessage, null, defaultMessage, locale);
    }

    private boolean stringNotNullOrEmpty(String s) {
        return !Objects.isNull(s) && !"".equals(s);
    }
}
