package com.spring.thanhnb.ch02._07;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@Configuration
public class I18NConfig implements WebMvcConfigurer {

    /**
     * The bean instance must have the name messageSource for application context detect it
     * <p>
     * messageSource.setBasename("classpath:message"); declare s String list via the setBasename() method to locate
     * for the ResourceBundleMessageSource. In this case, you just specify the default convention to look up files
     * located in the Java classpath the start with "message"
     * Đọc vào file i18n/messages_xxx.properties
     * Ví dụ: i18n/messages_en.properties
     * <p>
     * messageSource.setCacheSeconds(1); : set cached time avoid reading stale messages
     *
     * @return ReloadableResourceBundleMessageSource
     */
    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:i18n/messages");
        messageSource.setCacheSeconds(1);
        return messageSource;
    }


    /**
     * Method addInterceptors sẽ xác định thay đổi ngôn ngữ hiển thị thông qua param name nào trên trình duyệt.
     * Ví dụ trên URL có param: ?language=en thì tiếng anh sẽ được hiển thị
     *
     * @param registry:
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
        localeInterceptor.setParamName("language");
        registry.addInterceptor(localeInterceptor).addPathPatterns("/*");
    }

    @Bean(name = "localeResolver-c02")
    public LocaleResolver localeResolver() {
        final SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(new Locale("vi", "VIE"));
        return localeResolver;
    }
}
