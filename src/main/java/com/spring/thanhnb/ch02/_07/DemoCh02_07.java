package com.spring.thanhnb.ch02._07;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class DemoCh02_07 {

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/demo-07")
    public String getMessage(HttpServletRequest request) {
        return messageHandler.getMessage(request, "hello1", "default message");
    }
}
