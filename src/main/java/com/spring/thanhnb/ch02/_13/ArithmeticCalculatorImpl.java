package com.spring.thanhnb.ch02._13;

import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Component("ArithmeticCalculatorImpl")
@EnableAspectJAutoProxy
public class ArithmeticCalculatorImpl implements ArithmeticCalculator {
    @Override
    public double add(double a, double b) {
        System.out.println("----- ADD METHOD -----");
        return 0;
    }

    @Override
    public double sub(double a, double b) {
        System.out.println("----- sub METHOD -----");
        return 0;
    }

    @Override
    public double mul(double a, double b) {
        System.out.println("----- mul METHOD -----");
        return 0;
    }

    @Override
    public double div(double a, double b) {
        System.out.println("----- div METHOD -----");
        return 0;
    }
}
