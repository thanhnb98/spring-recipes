package com.spring.thanhnb.ch02._13;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ArithmeticCalculatorImpl.class);
        ArithmeticCalculator arithmeticCalculator =
                context.getBean("ArithmeticCalculatorImpl", ArithmeticCalculator.class);
        arithmeticCalculator.add(1, 2);
        arithmeticCalculator.sub(4, 3);
        arithmeticCalculator.mul(2, 3);
        arithmeticCalculator.div(4, 2);
//        UnitCalculator unitCalculator = context.getBean("unitCalculatorImpl", UnitCalculator.class);
//        unitCalculator.kilogramToPound(10);
//        unitCalculator.kilometerToMile(5);
    }
}
