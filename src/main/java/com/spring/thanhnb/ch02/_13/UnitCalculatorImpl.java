package com.spring.thanhnb.ch02._13;

import org.springframework.stereotype.Component;

@Component("UnitCalculatorImpl")
public class UnitCalculatorImpl implements UnitCalculator {
    @Override
    public double kilogramToPound(double kilogram) {
        System.out.println("----- kilogramToPound method ------");
        return 0;
    }

    @Override
    public double kilometerToMile(double kilometer) {
        System.out.println("----- kilometerToMile method ------");
        return 0;
    }
}
