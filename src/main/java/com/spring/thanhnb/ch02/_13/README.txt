#JointPoint: là một điểm của chương trình, là nhưng nơi có thể được chèn những cross-cutting concern, Chẩng hạn chúng ta
cần ghi log lại sai khi chạy methd nào đó thì điểm ngay sau method được thực thi là một Jointpoint.
Một Jointpoint có thể là một phương thức được gọi, một ngoại lệ được throw ra, hay một field được thay đổi.


Before: được thực hiện trước join point.
After: được thực hiện sau join point.
Around: được thực hiện trước và sau join point.
After returning : được thực hiện sau join point hoàn thành một cách bình thường.
After throwing : được thực hiện sau khi join point được kết thúc bằng một Exception.