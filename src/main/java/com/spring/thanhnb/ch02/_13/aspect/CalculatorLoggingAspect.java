package com.spring.thanhnb.ch02._13.aspect;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class CalculatorLoggingAspect {
    private Log log = LogFactory.getLog(this.getClass());

    @Before("execution(* *.*(..))")
    public void logBefore() {
        log.info("The method add() begins");
    }

    public void logAfterReturn(Object returnValue) throws Throwable {
        log.info("value return was " + new ObjectMapper().writeValueAsString(returnValue));
    }
}
