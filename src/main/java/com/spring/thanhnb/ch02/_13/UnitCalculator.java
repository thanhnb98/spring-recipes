package com.spring.thanhnb.ch02._13;

public interface UnitCalculator {

    public double kilogramToPound(double kilogram);

    public double kilometerToMile(double kilometer);
}
