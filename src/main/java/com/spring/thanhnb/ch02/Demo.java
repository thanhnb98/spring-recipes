package com.spring.thanhnb.ch02;

import com.spring.thanhnb.ch02._06.ShopConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Demo {

    @Autowired
    private ShopConfig shopConfig;

    @RequestMapping(value = "/demo")
    public void getConfig() {
        shopConfig.getConfigs();
    }

}
