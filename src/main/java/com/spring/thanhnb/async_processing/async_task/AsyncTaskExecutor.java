package com.spring.thanhnb.async_processing.async_task;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;


@Component
public class AsyncTaskExecutor extends WebMvcConfigurationSupport {

    /**
     * This method to config Default timout and config ThreadPoolTaskExecutor to impl multiple thread
     *
     * @param configurer
     */
    @Override
    protected void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        configurer.setDefaultTimeout(50000); //5s
        configurer.setTaskExecutor(threadPoolTaskExecutor());
    }

    /**
     * Config ThreadPoolTaskExecutor
     *
     * @return
     */
    @Bean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setThreadGroupName("mvc-executor");
        taskExecutor.initialize();
        return taskExecutor;
    }
}
