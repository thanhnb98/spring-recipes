package com.spring.thanhnb.async_processing.async_task;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequestMapping(value = "/demo-callable")
public class DemoCallableController {

    @RequestMapping(value = "/callable", method = RequestMethod.GET)
    public Callable<String> callable() {
        return () -> {
            for (int i = 0; i < 10; i++) {
                Thread.sleep(1000); //1s
            }
            return "someThings";
        };
    }
}
