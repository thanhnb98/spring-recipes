package com.spring.thanhnb.async_processing.async_intercepter;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AsyncInterceptor implements AsyncHandlerInterceptor {

    private static final String START_TIME = "startTime";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("-------- START PRE HANDLE --------");
        if (request.getAttribute(START_TIME) == null) {
            request.setAttribute(START_TIME, System.currentTimeMillis());
        }
        System.out.println("-------- END PRE HANDLE ----------");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("-------- START POST HANDLE -------------");
        long startTime = (long) request.getAttribute(START_TIME);
        long endTime = System.currentTimeMillis() - startTime;
        System.out.println("---------THREAD:" + Thread.currentThread().getName() + " COMPLETE: " + endTime + "----------");
        System.out.println("-------- END POST HANDLE ---------------");
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("--------- START afterConcurrentHandlingStarted method ----------------");
        long startTime = (Long) request.getAttribute(START_TIME);
        request.setAttribute(START_TIME, System.currentTimeMillis());
        long endTime = System.currentTimeMillis();
        System.out.println("Request-Processing-Time: " + (endTime - startTime) + "ms.");
        System.out.println("Request-Processing-Thread: " + Thread.currentThread().getName());
        System.out.println("--------- END afterConcurrentHandlingStarted method ----------------");
    }
}
